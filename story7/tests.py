from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest

# Create your tests here.
class story7(TestCase):
    def test_url_story7(self):
        response= Client().get('/goals/')
        self.assertEqual(response.status_code,200)

    def test_story7_using_goals_template(self):
        response=Client().get('/goals/')
        self.assertTemplateUsed(response, 'goals.html')
        
    def test_konten_goals(self):
        response = Client().get('/goals/')
        isi = response.content.decode('utf8')
        self.assertIn("My Goals", isi)